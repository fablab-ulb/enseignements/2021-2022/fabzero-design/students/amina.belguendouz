# **MUSÉE - Compte Rendu de la Visite des Enfants**

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%205/ph%20exterieure.jpg)

Cette semaine, nous avons accueillis une classe de 24 enfants âgé de 6-7 ans et leurs 2 professeurs pour une visite. La journée a commencé par une introduction de Victor Lévy sur ce qu'est le design. Pour ce faire, il a utilisé les objets dont ils sont familier : sac à dos, boite à tartine, puis les as comparer entre eux pour mettre en avant le signe que représente les objets.

[audio de la présentation](../audio/presentation-musee-vl.m4a).

Après ça, les enfants ont été réparti par groupe de 6, chacun avec un accompagnant et la visite est lancée.
Pendant 45 min chaque groupe a parcouru le musée en s'arrêtant sur des objets définis lors des séances précédente.

![](../images/musée/03.JPG)

## **Amina**
##### Mes observations
Mon rôle dans tout était de me déplacer de groupe en groupe pour observer comment les enfants agissaient, ce qui attire leur attention ou au contraire ce qui ne les intéressent pas.
Ce qui fonctionnait bien et qui était fait par chacun des accompagnants était de commencer par une série de question leur demandant de commenter ce qu'ils voient, ce qu'ils pensent que l'objet est,... suite à quoi l'accompagnant prenait le relais pour expliquer davantage l'histoire derrière. Ensuite une série de questions/responses et de commentaires avait lieu avec l'accompagnant mais aussi entre les enfants eux-même.
Une autre manière qui semblaient intéresser les enfants étaient de présenter les objets en les contrastant (souple/rigide etc...). Ils faisaient alors plus simplement des liens avec ce qu'ils connaissent déjà.
Pour finir, la durée de la visite était trop longue. L'ensemble des groupes commençaient à se dissiper aux bout de 25-30min.
##### Débrief
Au retour du temps de midi, on s'est installé dans la Plasticotek pour débriefer avec les enfants de la visite. Ce qui ressorti de cette discussion était leur frustration du fait de ne pas pouvoir toucher la matière, les textures. Un second point qu'ils semblaient regretter, c'est de ne pas pouvoir voir comment les objets sont fabriqués. Du coté positif, le fait de faire des parallèles avec les choses qu'ils connaissent, moule à gateau ou poche à douille comme image pour comprendre le processus de fabriquer, leur à beaucoup plus globalement.
Si je dois tirer en 3 points important : toucher la matière, comprendre comment c'est fait, explication simple et proche d'eux.

[audio du débrief](../audio/debrief-fin-de-seance.m4a)

## **Louise**

##### La visite avec les enfants
La visite s’est plutôt bien passée. J’ai commencé le tour par le début de l’exposition en leur introduisant Philippe Decelle qui est à la base de cette collection en leur demandant s’il y en avait parmi eux qui collectionnait des objets; cela a capté leur attention quand je leur ai demandé qui en avait une assez importante pour ouvrir un musée. Puis nous sommes entré dans la première grande salle et approché des lampes avec différentes bases. Pour leur expliquer l’objet je leur ai demandé qui jouait à la toupie. Beaucoup m’ont répondu puis et m’ont expliqué qu’ils avaient en plus différents accessoires pour par exemple la faire rouler plus vite. Je leur ai donc expliqué que pour ces lampes c’était pareil: un a une base à laquelle on ajoute des accessoires pour avoir une autre lampe. Ensuite nous nous sommes dirigé vers les chaises pour enfants empillables et je leur ai demandé à quoi cela leur faisait pensé. Ils m’ont presque tout de suite dit : à des lego. La réponse étant correcte, je leur ai expliqué pourquoi ces chaises pouvaient s’assembler comme des lego et qu’elles étaient détachables en deux parties: assise et pieds ainsi que le dossier. Dans la salle suivante je leur ai présenté le siège en forme de dent (ils n’ont pas su deviner à quelle forme ça leur faisait penser), la chaise extérieure très organique prévue pour bien s’assoir où ils ont remarqué les rainures pour évacuer les eaux de pluie et enfin la chaise homme. Je leur ai expliqué que la chaise était moulée sur le corps d’un homme et du coup ils se sont demandé si on lui avait coupé la tête ce qui était assez drôle. Leur expliquer le moulage a été plus compliqué par contre, leur accompagnatrice leur a expliqué que c’était comme un plâtre avec lequel on peut garder l’empreinte des mains mais je ne suis pas sûre qu’ils aient bien compris. Nous sommes enfin arrivé au podium avec les chaises en porte-à-faux et je leur ai demandé qu’est ce qu’il avait de commun entre ces chaises. Après m’avoir répondu qu’elles n’avaient que deux pieds, je leur ai fait faire l’exercice de la chaise contre un mur afin qu’ils comprennent la difficulté de réaliser ce genre de chaises. Nous sommes alors passé à la salle suivante et je leur ai demandé à quoi les objets de cette salle leur faisait pensé, ils n’ont pas tout de suite compris le lien avec l’espaces mais une fois la réponse donnée ils ont tout de suite faire des rapprochements comme par exemple la télé qui fait pensé à un casque de cosmonaute. Je leur ai ensuite demandé comment ils pensaient que l’on s’asseyait sur la Dondolo et je leur ai expliqué que l’on aurait une sensation d’apesanteur. Puis nous sommes passé devant les bureaux de Calka et je leur ai demandé où il y avait ce genre de bureau à leur avis et je leur ai expliqué l’anecdote du Président et ils ont dit que ce genre de bureau devait être très moche dans le bureau du Président. Enfin nous sommes arrivé devant les objets transparents, je leur ai demandé qu’elle était, d’après eux, la caractéristique principale de ces objets. Après avoir trouvé la transparence, je leur ai demandé si d’après eux ces objets étaient lourds et quand je leur ai dit que le grand fauteuil pesait 30kg et son moule 3tonnes ils n’en revenaient pas. En arrivant devant les gonflable, ils ont tout de suite fait le lien avec la piscine. Je leur ai expliqué qu’à l’époque ces objets ne se mettaient pas dans la piscine et je leur ai fait deviner dans quelle pièce cela pouvait aller. A ce point là ils commençaient à être de plus en plus dissipés, heureusement, nous arrivions aux objets détournés en mousse. Je leur ai demandé à qu’est ce qu’était selon eux ces grands brins d’herbe puis ai expliqué que tous ces objets étaient en mousse; ils avaient l’ai plutôt dubitatif avec les cailloux. Enfin, nous sommes passé devant la vitrine “mode” et je leur ai demandé qui avait des vêtements en plastiques; ils étaient assez intéressés par les lunette aux formes extravagantes. Puis nous avons vite fait un saut sur le podium minimaliste où je leur ai demandé en quoi était réalisé le service afin de leur montrer les formes/textures que le plastique pouvait imiter. Pour finir nous sommes passé devant les objets recyclés, ils étaient assez curieux des procédés de fabrication et ont été surpris devant la chaise imprimée en frigo.
Après la visite on est allé chercher des feuilles et de quoi écrire pour qu’ils puissent réaliser des dessins d’observation; les enfants couraient et criaient partout mais sont devenu beaucoup plus calmes une fois entrain de dessiner. La visite a été un peu longue pour eux; la limiter à 30min la prochaine fois serait mieux (temps d’attention max)
##### Débrief à la plasticotek
Après le diner, ils sont venus à la plasticotek et on leur a demandé ce qu’ils avaient aimé dans le musée et ce qu’ils auraient aimé avoir afin de mieux comprendre le musée. Les réponses allaient toutes dans le même sens: pouvoir toucher, pouvoir comprendre comment c’est fait.

## **Steph de Steamlab**
Pour Steph, l'introduction du début de journée était intéressant comme approche. En utilisant leur affaire (sac à dos, boite à tartine, ...) ils ont été impliqué dans la visite d'entrée de jeu.
Le division en petit groupe est crucial selon Steph pour mener à bien la visite.
Ce qui lui semble aussi important est de rebondir sur ce que les enfants disent lors de la visite, toujours dans ce soucis de les impliquer et de garder leur attention. Par contre la visite de ne doit pas excéder 30 minutes.
Ensuite le court moment de dessin a été, pour elle, intéressant comme approche pour que les enfants se concentrent sur l'objet. Il ne le regarde pas seulement mais 'l'analyse' pour le redessiner et donc cherche à le comprendre.
Le dernier point soulever par Steph est la grande frustration de ne pas pouvoir toucher les objets.

## **Victor**
Cette journée a commencé, pour Victor avec beaucoup d'appréhension, mais au fil de la journée et des interactions avec les enfants ça allait mieux.
Au cours de la visite, il remarque que la comparaison est très un très bon moyen de leur faire comprendre les choses. (moule à gateau-crème fraiche) C'est judicieux de relier les objets du musée avec les objets de leur quotidien.
Les éléments qui les ont attirés étaient les choses étonnantes du type de la chaise *Translation*.
Ce qui a marquer Victor également était leur curiosité pour comprendre 'comment c'est fait?' et pour la majorité des objets.
Ce qu'il en retire au delà de ce qui a déjà été dit est le nombre trop grand d'objet présenté. La liste doit être réduite. Et enfin l'impact du nom dans la comprehension de l'objet, il pourrait être une porte d'entrée.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%205/ph%20interieur.jpg)

## **Maxime**

Pour Maxime, la journée peut se diviser en 3 temps forts.

##### La rencontre avec les enfants.

La classe est arrivée vers 10:00 au musée. A leur arrivée, nous nous sommes regroupés dans la salle polyvalente, où Victor leur a fait une petite introduction sur ce qu'est le design et les raisons qui ont poussées les designers à créer leurs objets de tel ou telle manière. Pour cela, il a analysé avec eux certains objets qu'ils rencontrent au quotidien, comme des sac à dos, des chaises ou encore des boîtes à tartines.

##### La visite de l'exposition.

Une fois cette petite introduction terminée, nous avons séparé les 24 enfants en 4 groupes de 6 personnes.
Une fois les groupes formés, nous sommes chacun partis d'un côté pour faire visiter le musée aux enfants.
Afin de garder une certaine logique entre les différentes visites, nous avions convenu d'une série de 18 objets qu'il fallait présenter aux enfants.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%205/objets%20visite.jpg)

L'objectif de cette visite était de familiariser les enfants avec le design. Pour chacuns des objets de la liste ci-dessus, je leur posais quelques questions sur leurs formes et les raisons de ces formes, mais aussi sur leur méthode de fabrication.
Après 30min environ, j'ai commencé à perdre leur attention jusqu'à ce qu'ils se mettent à jouer dans le musée. J'ai donc décidé de passer à la deuxième phase de la visite qui consistait à les faire dessiner un ou plusieurs objets de leurs choix.
Cette partie créative était assez interessante car ils avaient tendance à se diriger vers les mêmes objets. Est-ce du à la forme des objets qui attire l'oeil ou juste à un effet de groupe au sein de la classe.

##### Le retour et l'avis des enfants.

Vers 13:00 après une pause midi bien méritée, on a demandé aux enfants de se réunir dans la Plasticotek à l'arrière du musée afin d'avoir leur retour sur la matinée qu'ils avaient passé au musée.
L'élément qui est le plus ressorti durant cet échange était leur envie de toucher et de manipuler la matière et les objets. Un des moments où ils étaient les plus actifs, c'était lorsque nous leurs avons fait passer un sachet contenant des grains de plastique, qui représente la matière première de quasi tous les objets du musée.
Un autre moment que j'ai trouvé assez interessant, c'est lorsque nous leurs avons permis de se lever à la fin et qu'ils ont enfin pu toucher aux objets imprimés en 3D sur la table au centre de la pièce. A cet instant ils se sont tous rués sur ces petits objets et les mis en lien avec ce qu'ils avaient vu le matin dans le musée.


## **Yan**
///

![](../images/musée/musee-decroly-1.jpeg)
