# **Explorations et Recherches**

## **Préambule**

Avant de parler du projet, je vais faire un petit retour en arrière pour expliquer un peu le contexte du projet.

L'option se déroule en deux temps et en deux groupes : les personnes n'ayant jamais participer à l'option auparavant (module 1) — *dont je fais partie* — et celles qui y ont déjà participé (module 3). Au cours des cinq premières semaines, les modules 1, nous avons suivi la formation FabZero pour nous permettre d'utiliser les machines du FabLab. Les modules 3, eux, travaillaient aux le musée et exploraient les premières idées de projets répondant aux questions suivantes :

```
"Comment intéresser des enfants de 6 à 12 ans au design ? Comment les rendre actifs pour leur faire comprendre les objets qu’ils viennent découvrir souvent pour la première fois ? Comment leur faire prendre conscience des différentes dimensions qui se cachent derrière chaque objet: le moment où il à été créé, sa fonction, l’éditeur, le créateur, la matière et la technique de fabrication ..."(catalogue du cours)
```

En première 'mise en bouche', au début du quadrimestre, nous avons visité le [Musée du Design de Bruxelles](http://www.plasticarium.be/home-museum-fr.html). Une visite pendant laquelle les modules 3 nous ont présentés le musée, son histoire et ses particularités.  

Par la suite, une visite avec les enfants de la classe de Yan a été organisée. Visite durant laquelle j'étais chargée de prendre note des éléments importants qui nous aideraient dans l'élaboration des projets de chaque groupe. [Ici](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/amina.belguendouz/kids@museum/musee-compte-rendu/) se trouve le compte-rendu.

A la fin des 5 semaines de la formation FabZero, il était temps de passer à l'étape de projet. D'abord, je me suis mise au courant de ce qui avait été fait par les modules 3 via leur git ([Louise](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/louise.vandeneynde/FabZero-Modules/module01/), [Tehjay](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/tejhay.pinheiroalbia/FabZero-Modules/module01/), [Maxime](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/), [Elliot](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/eliott.steylemans/)). Puis, il a fallu nous joindre à l'un projet qui nous intéressait. Mon choix s'est porté sur le projet de [Maxime](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/final-project/), qui s'intéresse à faire comprendre les méthodes de fabrication des objets en plastique par le biais de petit atelier de fabrication.

## **Semaine 1 // 08.11-14.11**

D'abord, pour concevoir des objets simples qui reprennent les principes de fabrications d' objets en plastiques, il fallait que je comprenne les différentes méthodes de fabrication. En suivant, les conseils de Maxime, j'ai regardé [cette émission de "C'est pas sorcier"](https://www.youtube.com/watch?v=irFEnEZhlNM) consacré au plastique et la [vidéo de Scilabus](https://www.youtube.com/watch?v=LqNnnsQxAFg) intitulée *"d'où vient le nombril des bouteilles?"*.

Comprenant mieux maintenant les techniques de fabrication, j'ai exploré ce qui a déjà été fait en termes de ré-interprétation des techniques industrielles à plus petite échelle. Je me suis concentrée sur les techniques que Maxime n'avait pas encore exploré complètement.

- ##### **Injection**

Pour la méthode d'injection, on doit avoir la forme négative de l'objet, ensuite on injecte de la matière qui prendra alors la forme voulu.

*image carnet*

Pendant mes recherches, je suis tombé sur cette [vidéo de Melkano](https://www.youtube.com/watch?v=CZYx35jiSrA&t=478s). Ce dernier utilise les outils de fabrication numérique pour la fabrication de son moule. Ayant les mêmes contraintes, on s'est inspiré de son travail, pour synthétiser un modèle plus adapté aux enfants.

![](../images/musee/projet/melkano-injec0.png) ![](../images/musee/projet/melkano-injec1.png) ![](../images/musee/projet/melkano-injec2.png)

Les accroches, avec des vis, qu'il réalise ne nous semblaient pas adéquates pour les enfants, on a donc modélisé un système d'accroche avec des petites tiges sur une partie du moule qui viennent s'imbriquer dans des trous sur l'autre partie du moule.

*modèle .stl étoile*

- ##### **Rotomoulage**

En continuant à explorer Youtube, pour voir ce qui existe déjà. Ces 2 vidéos de système de rotomoulage nous ont intéressées. ([vidéo 1](https://www.youtube.com/watch?v=0IsWzmWj-5o&t=1059s) & [vidéo 2](https://www.youtube.com/watch?v=Chorml4Ak7M&t=194s))

Le principe général est donc d'avoir 2 cadres en bois qui tournent autour de 2 axes, l'un vertical et l'autre horizontal. On vient alors fixer le moule au centre du plus petit cadre, de cette manière, il tournera sur lui-même selon les 2 axes.

*dessin carnet*

La différence entre les deux systèmes présentés dans ces vidéos est la méthode de rotation. Le premier repose sur une rotation à la main, le second a une manivelle avec tout un système d'engrenage.

*dessin carnet*

![](../images/musee/projet/roto-video1.png) ![](../images/musee/projet/roto-video2.png)

- ##### **Thermoformage**

Pour le thermoformage, c'est également une [vidéo de Melkano](https://www.youtube.com/watch?v=Ac1anjZfrdY). Il s'agit en finalité d'une version DIY de la [thermoformeuse Myaku](https://www.mayku.me/pages/what-it-is).

![](../images/musee/projet/melkano-thermo.png)

Il nous paraissait que fabriquer une telle machine avec une surface qui doit être chauffée assez fort pour des enfants n'était pas adapté.

## **Semaine 2 // 15.11-21.11**

Pendant la séance de discussion avec les différents groupes travaillant dans le musée, on a soulevé la question de nécessairement fabriquer les objets du musée avec nos modules de fabrication. Le fait de devoir représenter les objets présentait une contrainte qu'on ne parvenait pas à régler. On a donc décidé d'explorer la fabrication de forme beaucoup plus simple et d'utiliser une interface pour faire le lien entre les objets fabriqués par les enfants et les objets du musée.

Pour ce qui est de l'interface, le choix de faire l'atelier de fabrication avant la visite du musée est nécessaire. De cette façon l'accompagnant faisant la visite pourra alors se référer à ce qui a été fait pour expliquer la méthode de fabrication de l'objet.

- ##### **Rotomoulage**

Pour ce premier essai, j'ai dessiné l'ensemble des pièces à découper sur ArchiCad puis exporter le fichier en .svg. Pour ce qui est du dimensionnement de l'objet, ce dernier devant être manipulé par des enfants et les moules étant en taille réduite, il était inutile de faire un objet trop grand.

*image dessin*

Le matériau choisi a été le bois. On a récupéré une chute dans le FabLab, une planche de contreplaqué de 18 mm. Pour les découpes, on a utilisé la Shaper. On a fait la découpe en plusieurs passes de 6 mm. Cependant, on a rencontré un problème. La fraise était trop courte. Et on a dû finir la découpe à la scie sauteuse, le résultat n'était donc pas très propre. Il nous a fallu poncer pas mal pour retirer les éclats de la découpe.

![](../images/musee/projet/rotomoulage-collage3.jpg)

Ensuite, pour le système de rotation, on a utilisé une tige filetée, des rondelles, et des écrous. Puis on perce de bois pour y passer les tiges (légèrement plus large (5.5mm) que le diamètre de la tige (5mm)). Et on fixe l'ensemble.

Pour ce qui est du moule, comme on a décidé de passer à des objets plus simples et avec lesquels les enfants sont familiers, on a choisi de réaliser le moule d'une Pokeball.

*modèle stl v1*

L'impression terminée, je me suis rendu compte que j'avais oublié 2 choses importantes. La première, le trou pour verser la matière dans le moule, la seconde, les accroches pour fixer les 2 morceaux entre eux et pour fixer le moule au cadre.

![](../images/musee/projet/moule-gris.JPG)

En termes de matière pour le rotomoulage, on pense au plâtre.

- ##### **Injection**

A nouveau, pour cette méthode, on s'est replié sur un moule plus simple, en forme d'étoile.

*modèle*

Pour la matière, on s'est inspiré de la vidéo de Melkano présentée plus haut, on a utilisé un pistolet à colle. On a rencontré un problème c'est le temps de séchage. Nous avions attendu trop longtemps et c'était impossible de démouler l'objet sans casser le moule.

![](../images/musee/projet/injection-collage.jpg)

![](../images/musee/projet/injection-collage2.jpg)

- ##### **Extrusion**

Pour cette technique à nouveau, on a modélisé des formes plus simples. Et on a utilisé de la pâte Fimo comme matière car elle est suffisamment malléable pour l'extruder aisément.

*capture modèle*

##### Présentation à Yan - 18.11

Suite à la présentation à Yan de ce qu'on envisageait de faire, plusieurs choses ont été discutées.  

## **Semaine 3 // 22.11-28.11**

La semaine précédente, j'ai pris contact avec une amie d'enfance institutrice en France, Marline, pour avoir son expertise face au projet. Ces conseils ont été les suivants :

  - manipuler est une bonne chose pour les enfants
  - présenter que quelques de techniques pour ne perdre les enfants
  - faire un travail préalable à la visite et à l'atelier pour sensibiliser les enfants
  - faire l'atelier avant la visite du musée
  - faire une "chasse au trésor: *Je suis un objet de la salle, voici le moule qui a servi à ma fabrication, qui suis-je? — À ton avis, pour me fabriquer, on a utilisé quelle technique : le soufflage? ou l'injection? — Voici un objet de la salle, dessine le moule que l'on a utilisé pour le fabriquer. — Ou alors mettre des chaises d'un côté et des moules de l'autre et demander aux enfants de les relier.*

Avec ces conseils et les retours de la séance avec Yan, on a réfléchi aux objets du musée que l'on pourrait choisir pour chacune des techniques qu'on veut proposer ; à savoir : l'injection, l'extrusion et le rotomoulage.

  **Injection**

    - Umbo - Kay LeRoy Ruggles
    - H-Horse - Nendo (Kartell)

![](../images/musee/projet/bibliotheque.png) ![](../images/musee/projet/hhorse.png)

  **Extrusion**

    - Lampadaire Chimera Artemide - Vico Magistretti
    - Dondolo – Cesare Leonardi / Franca Stagiy

![](../images/musee/projet/lampadaire.png) ![](../images/musee/projet/dondolo.png)

  **Rotomoulage**

    - Molar Chair - Wendell Castle
    - La Bohème - Philippe Starck

![](../images/musee/projet/molar-chair.png) ![](../images/musee/projet/la-boheme.png)

Avant de nous lancer dans de nouvelles modélisations, nous avons décidé de tester tout les différents matériaux auxquels on pensait, pour voir ce qui était possible ou non. Et si les tests ne fonctionnaient pas avec des formes simples, ça ne fonctionnerait pas avec des moules plus complexes.

- ##### **Rotomoulage**

**Le plâtre**

Pour le rotomoulage, la première matière qu'on a testé est le plâtre. Pour chacun des tests on a recouvert les moules d'une couche 'lubrifiante' pour faciliter le démoulage (WD40 puis huile de coco). Mais les tests n'ont malheureusement pas été concluants. Malgré une bonne répartition de la matière dans le moule, le temps de séchage était beaucoup trop long.

Pour le premier test, on avait mis beaucoup trop de matière et au moment de démouler la Pokéball s'est cassée. Le plâtre avait bien pris sur les parois du moule mais n'était absolument pas solide. Après avoir laissé sécher toute une nuit, le plâtre n'était toujours pas sec. Notre première intuition était que la couche était trop épaisse pour sécher. on a donc fait un second test avec une couche plus fine. A nouveau, le plâtre ne séchait pas. On a donc abandonné l'idée.  

![](../images/musee/projet/rotomoulage-collage4.jpg)

![](../images/musee/projet/roto-test-platre)

![](../images/musee/projet/rotomoulage-collage5.jpg)

**La cire**

La deuxième matière qu'on a testé est la cire. On a préféré faire ce test à la main pour aller plus vite. Cette fois ça a fonctionné, le temps de séchage est suffisamment court. Cependant lors du démoulage l'objet s'est cassé une première fois. Les 2 parties sont restées coller aux parois du moule car on avait oublier de recouvrir le moule d'huile. On a donc refait un test. A nouveau l'objet c'est cassé. On pense donc que le moule doit donc être découpé en 4 parties et non 2 pour éviter les casses.

![](../images/musee/projet/test1-cire.gif)

![](../images/musee/projet/rotomoulage-collage2.jpg)

- ##### **Injection**

**Le pistolet à colle**

Après le premier test raté, on a re-tenté l'expérience du pistolet à colle avec une nouvelle modélisation. Cette fois-ci, le moule est plus fin et les accroches viennent autour des 2 parties moules pour maintenir l'ensemble avec la pression de la matière entrant dans le moule. Ayant appris de notre erreur, cette fois, le temps de séchage était beaucoup moins long mais le pistolet utilisé était différent et a fait fondre l'embouchure du moule. Mais lors du second essai avec l'ancien pistolet à colle, tout était ok. Donc ça fonctionne mais ce n'est clairement pas optimal.

![](../images/musee/projet/injection-collage3.jpg)

- ##### **Extrusion**

Pour l'extrusion, les précédents tests de Maxime avec la pâte à sucre n'ayant pas fonctionné (matière trop dure et moule pas adapté), on s'est tourné vers la pâte Fimo. On a trouvé une pâte qui se cuit au micro-onde en 10 min. On a alors vérifié que la pâte s'extrude convenablement dans nos moules et c'est le cas.

![](../images/musee/projet/extrusion-collage.jpg)

![](../images/musee/projet/extrusion-collage3.jpg)  

## **Semaine 4 // 29.11-05.12**

Au-delà du choix de 3 méthodes seulement, nous nous sommes imposés une nouvelle contrainte, celle de l'utilisation d'une seule matière : la cire. L'utilisation d'une unique matière permet également de faire le parallèle avec le plastique utilisé pour toutes ces méthodes de fabrication. Selon le stade fonte de la matière, on peut la travailler en pâte (extrusion) ou liquide (injection et rotomoulage).

Pour la venue des enfants, nous nous sommes concentrés sur la modélisation de nouveau moule de la forme des objets.

- ##### **Rotomoulage**

Pour le moule du rotomoulage, on a préféré conserver le moule de la Pokeball et découper ce dernier en 4 parties pour un démoulage plus facile. Petit problème rencontré, l'impression finie, le décalage entre les accroches et les parties du moules n'était pas suffisant. On a donc poncé les tranches du moules pour pouvoir assembler le moule.

![](../images/musee/projet/moule-4-parties.jpg)

Pour ce qui est de la structure, on a re-découpé l'ensemble car la première version présentait quelques soucis. Les angles de la première version sont trop abruptes, les arrondir est nécessaire pour limiter les blessures. Ensuite, pour stabiliser l'ensemble lors des rotations on a élargi la base et ancré les pieds dans cette dernière.

![](../images/musee/projet/rotomoulage-collage6.jpg)

Après avoir terminer le prototype, on a réalisé un nouveau test avec la cire, cette fois-ci dans avec la rotation de la machine.

![](../images/musee/projet/roto-test-cire.gif)

![](../images/musee/projet/rotation-test-cire.gif)

![](../images/musee/projet/demoulage-cire.gif)

- ##### **Injection**

Pour l'injection, on a décidé de changer la forme créée pour l'un des objets exposés dans le musée. Notre choix s'est porté sur la bibliothèque Umbo, l'idée étant que les enfants puissent créer plusieurs morceaux du modules en T et L pour ensuite les empiler pour reformer la bibliothèque.

*modélisation moule*

- ##### **Extrusion**

Comme pour l'injection, on voulait créer des moules qui puissent recréer les objets du musée choisis. L'impression 3D fonctionne pour l'extrusion de forme simple mais pour les objets du musée, les précédents tests de Maxime n'étaient pas réussis. On a donc choisi de changer de méthode. Cette fois avec un 'contenant' dans lequel vient se glisser une plaque extrudant la forme qu'on souhaite.

*dessin*

![](../images/musee/projet/extrusion-collage2.jpg)

On a donc dessiné ces plaques et on a choisi de les découper dans du plexiglass de 4mm car plus rigide.

*capture dwg*

*photo découpe*

On a alors tenté d'extruder la cire du moule et la tentative n'a pas été très réussie. Le rectangle était trop large et la matière s'étalait dans le fond sans en sortir. Pour avoir un meilleur résultat, on devait utiliser beaucoup trop de matière. Et le recul n'était pas suffisant.

![](../images/musee/projet/malaxage-pate-min.gif) ![](../images/musee/projet/extrusion-min.gif)

Pour la modélisation suivante on a donc opté pour une forme ronde qui fonctionne mieux (au vu de nos tests précédents), également, on a allongé le module pour avoir le plus de recul possible.

##### **Crash-test avec les enfants // 02.12**

A la suite de la visite des enfants au FabLab pour un premier test des prototypes, [ici]() se trouve le compte-rendu de ce crash-test.

![](../images/musee/projet/enfants1.jpg) ![](../images/musee/projet/enfants2.jpg)

## **Semaine 5 // 06.12-12.12**

Cette semaine, le test des premiers prototypes avec les enfants a soulevé un certain nombre de points à revoir. Je vais les énoncer par méthode de fabrication.

- ##### **Rotomoulage**

Cette méthode était leur préférée, la plus ludique et celle qui a fonctionné le mieux. Cependant, le résultat n'était pas suffisamment fiable : toutes les tentatives qu'on a fait n'ont pas abouti à un objet complet. On a rencontré 2 problèmes majeurs :

- la quantité que l'on versait était assez aléatoire parce que la cire séchait dans le fond du verre doseur donc la quantité n'était plus suffisante. Après avoir constaté ce souci, on ajoutait une petite quantité supplémentaire de cire et alors, la couche trop épaisse ne séchait pas et se cassait lors du démoulage.
- la rotation à la main des 2 cadres par les enfants était un peu trop aléatoire pour que la cire se répartisse de manière homogène dans le moule.
- la stabilité de la structure, malgré la base plus large, certains enfants tapaient trop fort sur les cadres, on a donc dû le fixer à l'établi avec des serre-joints.

![](../images/musee/projet/pokeball-resultat.JPG)

Les solutions à ces 2 problèmes sont d'abord, de faire chauffer le récipient avant d'y verser la cire ce qui permet d'être plus précis pour les quantités. Ensuite, pour la rotation la solution est d'ajouter une manivelle qui rendra la rotation plus homogène. Enfin pour la stabilité, on va élargir à nouveau la base et l'épaissir pour l'alourdir.

*image*

*doc .svg*

Pour réaliser la manivelle, on se base sur le modèle présenté plus haut. On modélise les engrenages pour l'impression 3D, ces engrenages passent la rotation selon un axe horizontal du cadre extérieur vers une rotation selon un axe vertical du cadre intérieur.

*dessin carnet*

- ##### **Injection**

Pour l'injection, l'impression des modules de la bibliothèque n'ayant pas fonctionné (*l'échelle réduite pour diminuer le temps d'impression a rendu le moulage de la cire impossible car elle séchait avant de pouvoir se répartir dans tout le moule*), on a décidé d'utiliser notre ancien moule pour le test des enfants. On a alors remarqué un problème majeur, le démoulage. Un problème que nous avions déjà réglé pour le moule de la bibliothèque réalisé en 3 parties.

J'ai donc ré-imprimé un des deux moules seulement et non plus les deux en même temps et j'ai tout de même réduit l'échelle à 50% sur PrusaSlicer pour réduire le temps d'impression.

*modèle et capture Prusa*

J'ai ensuite fait un test avec la cire qui est réussi. Cependant, l'objet est relativement fin, la prochaine étape est donc d'élargir l'objet sur Fusion pour avoir un objet suffisamment épais pour empiler tous les modules de cire.

![](../images/musee/projet/injection-collage4.jpg)

*nouvelle modélisation*

- ##### **Extrusion**

L'extrusion a été la méthode avec laquelle on a rencontré le plus de problème. Le principal étant la cire qui durcissait beaucoup trop vite. Après l'avoir formé en pâte, la fenêtre de malléabilité était beaucoup trop courte pour que les enfants puissent pousser la pâte dans le moule.

La solution à ce problème est pour nous a été d'abandonner la cire pour la pâte Fimo qui a été prometteuse lors de nos précédents tests. Egalement, on a pensé ajouter un levier qui permettrait de décupler la force et extruder la pâte plus aisément.

- ##### **Scénario**

En parallèle aux différents tests et améliorations, on a avancé sur le scénario d'usage de nos différents ateliers.

*capture doc*

Pour ce qui est du fonctionnement des différentes méthodes de fabrication, pour rendre le processus clair, on a décidé de mettre en place des modes d'emploi pour chacune des méthodes. En s'inspirant du modèle des notices Ikea, chacune des étapes est illustrée en veillant à avoir un minimum d'étape pour faciliter la compréhension.

**Extrusion**

![](../images/musee/projet/guide-extr.png)

**Injection**

![](../images/musee/projet/guide-injec.png)

**Rotomoulage**

![](../images/musee/projet/guide-roto.png)

## **Semaine 6 // 13.12-16.12**
