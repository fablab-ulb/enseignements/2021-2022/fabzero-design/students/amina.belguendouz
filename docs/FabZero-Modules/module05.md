# **MODULE 05_Usinage assisté par ordinateur**

Pour cette semaine, on a appris à utilier la machine [Shaper Origin](https://www.shapertools.com/fr-fr/#). Il s'agit d'une fraiseuse CNC portative de précision, guidée à la main.

## Petite présentation de la machine

#### Quelques informations

![](../images/module-05/shaperorigin.jpg)

Cet outil permet de découper des formes de manière très précise dans la matière. La machine se manipule à la main et est capable de corriger notre trajectoire grâce au dessin, affiché sur l'écran, que l'on paramètre à l'avance.

![](../images/module-05/schema.jpg)

La machine se compose d'un corps principal avec une poignée pour la transporter. On y trouve un port usb pour importer notre fichier de découpe. Ensuite les éléments qui coupent la fraise sur son mandrin. Un écran tactile au dessus nous permet de paramétrer et visualier la découpe. Deux poignées latérales pour prendre en main la machine pendant la découpe et où se situent de respectivement un bouton de démarage et un d'arrêt de découpe. Enfin, au dos, se trouve une caméra avec lumières LED pour se situer la machine sur le matériau. En effet, pour se situer la machine nécessite des repères : le ShaperTape.

Quelques spécifications :

- Profondeur de découpe max. : 43 mm
- Diamètre du collet : 8 mm ou 1/8"
- Format de fichier supporté : SVG

On peut retouver [ici](https://www.shapertools.com/fr-fr/origin/spec), une description détaillée de la machine et [là](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf) le manuel d'utilisation.

#### Les précautions d'usage

Comme pour toutes les machines présentées jusque là, il y a un certain nombre de chose à connaitre avant de se lancer dans l'utilisation.

- Utiliser la machine sur un plan de travail stable
- Toujours fixer le matériau sur le plan de travail à l'aide de double face, serre-joint ou vis
- Ne pas oublier d'allumer l'aspirateur à poussières
- Être dans une position stable, équilibrée et confortable pour l'utiliser, et veiller à ce que l'ensemble de la découpe soit à notre portée
- Éviter les vêtements larges, bijoux, ou cheveux longs lâchés

#### Les réglages

##### la vitesse et la qualité de découpe
La machine propose différentes vitesses allant de 1 à 6, ce qui correspondent à une vitesse de 10 000 à 26 000 tours/minute. Le choix de la vitesse de fraisage dépend du matériau et de son épaisseur. Quelques petits indicateurs :
- si une découpe trop lente ne sera pas lisse et laissera apparaître des à-coups,
- si elle est  trop rapide brûlera ou écorchera le bois.

Un indicateur de vitesse est la taille des copeaux lors de la découpe, la taille normale des copeaux est d'environ 1mm, s'ils sont plus gros c'est que la vitesse est trop lente, plus petits alors trop rapide. Elle se règle sur l'écran tactile.

Le plus sage est probablement l'option de vitesse automatique. Elle permet à la machine de guider elle-même la découpe, auquel cas il faut pouvoir la suivre malgré tout mais au moins on évite donc des vitesses trop rapides.

Il existe plusieurs fraises de différentes tailles pour différents matériaux et épaisseurs. [Ici](https://support.shapertools.com/hc/fr-fr/articles/360016398434-Recommandations-de-paramètres-par-matériau) le tableau récapitulatif.

##### l'épaisseur de la fraise

Il est important de prendre en compte l'épaisseur de la fraise dans la découpe et le tracé. C'est un paramètre que l'on peut paramétrer, il en existe plusieurs type :

- *inside* : découpe à l'intérieur du tracé,
- *outside* : découpe à l'extérieur du tracé,
- *on line* : découpe sur le tracé
- *pocket* : coupe par évidage

![](../images/module-05/typedecoupe.jpg)

Il est possible de réaliser 2 à 3 passes afin d'obtenir une découpe propre et homogène. Il est notamment possible de régler un décalage de la découpe par rapport au tracé, offset qui permet par exemple d'évider les coins.

##### le ShaperTape

Revenons un instant sur le ShaperTape. C'est donc le moyen pour la machine de se repérer mais comment le placer ?

![](../images/module-05/shapertape2.jpg)

Il faut disposer des bandes parallèles, tous les 8cm et de minimum 4 rectangles, devant la machine qui  les scanner grâce à sa caméra et donc définir son espace de travail.

***Attention ! Il faut veiller à ce que le ShaperTape couvre toute la zone que l'on prévoit de découper et devant cette zone afin que la machine capte toujours ces marqueurs.***

## Utilisation

Passons à l'utilisation. Le Fablab a constitué un [guide](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/Shaper.md) complet sur l'utilisation. En complément voilà quelques [tutos vidéos](https://www.shapertools.com/fr-fr/tutorials) mis en place par le fabriquant.

Ici, pour présenter le prcessus, je vais utilser la découpe qu'on a réalisé lors de la formation : une planète dans une planche de bois.

##### le dessin

La machine, similairement à la découpeuse laser, nécessite une image vectorielle c'est à dire des images .svg. De la même manière que la semaine dernière, il faut réaliser notre dessin sur un logiciel de tracés vectoriels comme Illustrator ou Inkscape.

***Attention ! Pour une découpe, assurez-vous que votre tracé soit bien fermé, sinon la machine n'est pas capable de faire une découpe complète de l'objet.***

##### la découpe

Afin de préparer la découpe il faut, comme expliqué plus haut, le matériau doit être bien fixer au plan de travail. Ensuite on place le ShaperTape comme expliqué avant (bandes parallèles séparées de 8cm max.et min. 4 rectangles) et enfin on met la fraise adaptée dans la machine.

![](../images/module-05/shapertape.jpg)

{ *Pour changer la fraise, il faut tout d'abord appuyer le bouton de verrouillage de la broche, puis dévisser la vis à l'aide de la clé T-Hexagonale afin de retirer la broche.* }

Après avoir insérer la clé USB dans le slot, on clique sur *'nouveau scan'* dans le menu *'scan'* à droite de l'écran,. Il faut ensuite bouger la machine de façon à scanner tout le ShaperTape qui devient bleu quand il est reconnu.

![](../images/module-05/0scan.jpg)

Il est possible de dessiner des formes simples directement sur la machine depuis le menu *'dessiner'*. Cet outil peut être utile pour effectuer des tests de calibrage de vitesse par exemple.

![](../images/module-05/1dessin.jpg)

Pour accéder au dessin chargé sur la clé USB, on clique sur *'importer'*. Une fois notre fichier sélectionné, on peut visualiser son emplacement et la place qu'il prendra sur l'espace de travail. Si on le souhaite on peut changer son échelle, le tourner etc. Le curseur à droite permet d'agrandir ou rétrécir le point de vue sur l'espace de travail. Une fois satisfait de sa taille et emplacement, on clique sur *'placer'*.

![](../images/module-05/2importer.jpg)

***En haut à droite de l'écran, un indicateur permettant de savoir si l'on se trouve dans l'espace de travail possible. Si c'est pas le cas, le voyant apparaît en rouge. S'il est en gris et noir c'est qu'on y est presque, et lorsque l'on est bien placé, il s'affiche en noir.***

Dans le menu de droite, on peut passer maintenant à l'étape *'fraiser'*. Sur la gauche, on a les différents réglages : la profondeur de fraisage (ici sur 6mm), le type de découpe (ici sur outside), l'offset (ici à 0), la dimension de la fraise etc. On s'assure également que toute la découpe est à notre portée, que l'on soit pas dans une position instable pendant la découpe. Lorsque tous les paramètres sont enregistrés on clique sur *'fraiser'*.

![](../images/module-05/3placer.jpg)

Avant d'effectuer la découpe, il ne faut pas oublier d'allumer l'aspirateur relié à la machine pour limiter les coppeaux, et d'allumer également la broche : bouton on/off.

![](../images/module-05/4fraiser.jpg)

Pour lancer la découpe, on appuie une fois sur le bouton vert sur la poignée de droite. Si on reste appuyé, la machine passe en vitesse automatique.

![](../images/module-05/5boutonvert.jpg)

On suit la trajectoire affichée sur l'écran en suivant **le sens de la flèche**. La partie du tracé déjà découpée apparaît alors en bleu. Si le cercle blanc sort du tracé, la machine arrête automatiquement la découpe et le mandrin se relève. Pour arrêter la découpe soi-même, on appuie sur le bouton rouge sur la poignée de gauche.

![](../images/module-05/6boutonrouge.jpg)

***Important! On ne peut pas enchainer les découpes de formes disctinctes. Entre chaque forme ou tracé différent, il faut arrêter le fraisage et relever le mandrin (bouton rouge).***

Une fois la découpe terminée, il faut bien éteindre la broche en poussant le bouton sur off.

![](../images/module-05/on-off.jpg)

Sur la photo de notre découpe en dessous, on voit que pour la première découpe, les paramètres n'était pas bon et la découpe était donc pas assez profonde par rapport à l'épaisseur du matériau. La fraise n'a pas de transpercer la planche et donc pas de découpe en une passe. En tout logique on a ensuite régler le fraisage sur une plus grande profondeur et là tout c'est passé comme sur des roulettes.

![](../images/module-05/objet.jpg)

## Liens utiles

- [Site ShaperOrigin](https://www.shapertools.com/fr-fr/#)
- [Spec. machine](https://www.shapertools.com/fr-fr/origin/spec)
- [Manuel Utilisateur](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf)
- [Tableau des recommendations par matériaux](https://support.shapertools.com/hc/fr-fr/articles/360016398434-Recommandations-de-paramètres-par-matériau)
- [Guide du FabLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/Shaper.md)
- [Vidéo tutos](https://www.shapertools.com/fr-fr/origin/spec)
