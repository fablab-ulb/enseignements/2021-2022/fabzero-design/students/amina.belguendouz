# **MODULE 02_Conception Assistée par Ordinateur**

Cette fois, on s'est attaqué à [Fusion360](https://www.autodesk.com/products/fusion-360/personal), un outil de modélisation 3D.

## **Se familiariser à l'interface**

![](../images/module-02/interface.jpg)

Les éléments en sur-brillance rouge sont les outils de navigation.

- En haut à gauche : le navigateur, on y retrouve tous les éléments du projet (corps, esquisses, vues,...)
- En bas à gauche : l'arbre de construction, c'est un élément important. Il reprend toutes les étapes de construction du modèle et permet d'apporter une modification aux étapes précédentes sans perdre la suite de la construction.
- En bas au milieu : on y retrouve tous les outils d'affichage et de navigation 3D.
- En haut à droite : le cube permet également de naviguer en 3D sur le modèle en sélectionnant les faces ou encore les coins de celui-ci.

## **Les outils**

Je vais maintenant présenter les outils principaux qui m'ont été utile à la modélisation de mon objet.

1. *créer une esquisse* : pour dessiner en 2D, il faut d'abord créer une nouvelle esquisse (*bouton sur-brillance bleue sur l'image n°1*). Dès lors, il faut choisir le plan sur lequel on veut dessiner, cliquant sur l'une des faces du carré au centre de l'image.
2. *les outils de dessin 2D* : après avoir choisi sur quel plan on dessine. Un grand nombre d'outils, sous différents onglet sont disponible.

 * *créer* : comme son nom l'indique, c'est là qu'on retrouve tous les éléments que l'on peut créer ; des lignes, des rectangles, cercles, etc.
 * *modifier* : à nouveau, assez explicite, on peut trouver des outils pour couper, prolonger, décaler, etc. tous les éléments déjà dessinés.
 * *contrainte* : cet onglet permet d'appliquer des contraintes dans le dessin, permettant de dessiner au plus juste comme par exemple s'assurer de dessiner des parallèles, etc.
 * *inspecter* : avec cet outil ont peut mesurer des distances ou des angles sur le dessin.

***Point important, lorsque le dessin 2D est terminé, il faut fermer l'esquisse en cliquant sur la coche verte en haut à droite.***

![](../images/module-02/esquisse.jpg)

3. *les outils de dessin 3D* : ces esquisses 2D vont nous servir pour modéliser en 3D. Sous l'onglet 'créer' on retrouve :

 * *extrusion* : on peux extruder (E) l’objet 2D via l'option du menu créer. Là le menu de droite demande de sélectionner le dessin 2D à extruder mais également un tas d'autres option dont : le point de départ, la direction de l'extrusion (à sens unique, à double sens ou symétrique), le point de fin, la distance de la extrusion, l’angle et enfin l’opération (joindre, couper, croiser, nouveau body, nouvelle composante)
 * *révolution* : une autre manière de créer un objet en 3D  est l’outil ‘révolution’. Avec cet outil, on peut faire tourner un profil 2D autour d’un axe sélectionné.
 * *balayage* : comme son nom l'indique, l'outil permet de balayer un profil 2D le long d’un chemin choisi. Pour ce faire, il faut sélectionner le profil en question puis la ligne définissant le chemin.
 * *réseau* : la dernière que je vais présenter mais pas la moindre, cette fonction permet de dupliquer des extrusions, faces, corps,... selon un réseau rectangulaire, circulaire ou une trajectoire souhaitée.

![](../images/module-02/extrusion-réseau.jpg)

## **Méthodologie de modélisation**
Pour rappel voici l'objet choisi au musée.

![](../images/module-02/giotto-stoppino-magazine-rack.jpeg)

### **Méthode 01 - échec**

Pour modéliser cet objet, j'ai choisi d'utiliser l'option balayage. J'ai d'abord dessiné le chemin du profil puis le profil.
Ensuite il fallait dupliquer l'élément pour former la moitié de l'objet. J'ai déplacé ces éléments ensuite à la vertical en utilisant la fonction déplacer. Enfin j'ai de nouveau dupliquer le tout avec la fonction miroir en fonction de vertical.
Et voila l'objet est créer.

![](../images/module-02/screen-record.mp4)

### **Edit Update - Méthode 02 - réussite**

Au moment de l'impression j'ai pris conscience que mon modèle dessiné tel qu'il l'était, n'avait pas de fond et ces tranches ne se superposaient pas convenablement. J'ai alors réfléchit à une autre méthode poir dessiner l'objet.
Cette fois on créé d'abord une surface correspondant à un module du porte revue.

![](../images/module-02/update/00-esquisse.png)

Ce rectangle arrondi dessiné il faut l'extruder (ici, h=240mm). Cool! Maintenant on a un bloc plein mais il faut l'évider.

![](../images/module-02/update/01-extrusion.png)

Pour faire ça, on utilise la fonction 'coque', elle nous permet de paramétrer l'épaisseur extérieur voulue ; ici 5 mm (*épaisseur minimum pour l'impression 3D de l'objet — dont l'échelle est réduit — fonctionne*). Voila, on a créé un des 6 modules qui forment le porte-revue.

![](../images/module-02/update/02-coque.png)

Comment le démultiplier maintenant ? La fonction réseau. Elle nous permet de démultiplier le module sur l'axe que l'on veut, ici x (ou axe rouge). On le démultiplie 6 fois, on se trouve donc avec l'ensemble des 6 modules à la même hauteur en s'assurant que les 'tranches' se superposent bien. Ici, avec un module de 50mm de largueur et 5mm d'épaisseur de coque l'étendu est de 225mm). A la différence du dernier modèle pour lequel j'ai réalisé une moitié de l'objet pour ensuite le dupliquer avec la fonction miroir, cette méthode (avec le réseau rectangulaire) je parviens mieux à maîtriser la superposition des tranches.

![](../images/module-02/update/03-reseau.png)

Nous voilà donc avec une rangée de modules dont il faut ajuster la hauteur. Pour le coup, rien de plus simple, on prend l'outil déplacer puis on sélectionne les 2 modules auxquels on souhaite affecter une certaine hauteur (ici, y=100mm et pour les plus haut y=200mm). Voilà, l'objet est quasi fini.

![](../images/module-02/update/04-hauteur01.png)
![](../images/module-02/update/05-hauteur02.png)

Une dernier étape reste à faire. Il faut combiner les éléments pour avoir un unique objet et faire disparaitre les lignes de superposition.

![](../images/module-02/update/06-non-combine.png)

On prend la fonction combiner puis on sélection l'ensemble des corps formant l'objet puis on applique la fonction.

![](../images/module-02/update/07-combinaison.png)

Tada! l'objet est fini.

![](../images/module-02/update/08-objet-fini.png)

![](../images/module-02/update/screen-record-final.mp4)

## **Paramètres**

Un dernier point sur lequel des modifications ont été apporté est celui du paramétrage de l'objet. La fonction 'paramétrer' permet d'apporter des modifications plus efficacement à l'objet. On accède au menu de paramétrage via l'onglet '*modifier*' puis '*modifier les paramètres*'.

![](../images/module-02/update/ouvrir-parametre.png)

Là, une nouvelle fenêtre s'ouvre, on y trouve :

 * '*les paramètres utilisateur*' : ce sont les paramètres qu'on créé pour le projet.
 * '*les paramètres modèle*' : là, on retrouve tous les paramètres des différentes fonctions du dessin.

J'ai donc configurer les différents paramètres de l'objet. En cliquant sur le + à coté des *paramètres utilisateur*'. On nomme ce dernier et affecte des caractéristiques.

![](../images/module-02/update/ajouter-parametres.png)

Puis on applique ces derniers paramètres aux fonctions du dessin, en renommant la valeur avec le paramètre créé plus haut.

![](../images/module-02/update/affecter-parametre.png)

Voilà le tableau qui en résulte :

![](../images/module-02/update/parametres-utilises.png)

Pour une explication plus complète, voilà un [tuto](https://www.youtube.com/watch?v=90bvJHHCd24).

## **Le modèle 3D**

<iframe src="https://myhub.autodesk360.com/ue2b72b17/shares/public/SH9285eQTcf875d3c53993dffcbb54a26620?mode=embed" width="800" height="600" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

Afin d'importer le modèle 3D téléchargeable ci-dessus j'ai suivi ce [tuto](https://www.youtube.com/watch?v=lWDh7XZASoA).
Le fichier .stl est également disponible via ce [lien](../images/module-02/update/porte-revue-paramétré-final.stl).

## **Liens utiles**

- [Fusion360](https://www.autodesk.com/products/fusion-360/personal)
- [Tutoriel sur les paramètres sur Fusion360](https://www.youtube.com/watch?v=90bvJHHCd24)
- [Tutoriel 'imbedded model'](https://www.youtube.com/watch?v=lWDh7XZASoA)
- [Télécharger le modèle format .stl](../images/module-02/update/porte-revue-paramétré-final.stl)
