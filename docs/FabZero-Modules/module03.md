# **MODULE 03_Impression 3D**

Cette semaine, on s'est attaqué à l'impression 3D. Je vais d'abord expliquer les choses principales à connaître sur les imprimantes du FabLab. Ensuite je partagerai mon expérience lors de l'impression du [modèle](../images/module-02/update/porte-revue-paramétré-final.stl) du module précédent.

## **L'imprimante 3D : Prusa**

### Présentation
Le FabLab est équipé d'imprimantes Prusa I3 MK3S. La machine tient son nom de son créateur Josef Prusa à la tête de la startup du même nom Prusa Research. Leur travail puise sa base dans le projet RepRap (contraction de *Replication Rapid Prototyper*). Ce projet a pour objectif de créer une imprimante 3D capable de s'auto-répliquer, c'est-à-dire capable d'imprimer les pièces qui la compose, mais surtout d'accès libre. Le partage en open-source des codes sources, plans, et des différentes pièces rend accessible à tous la construction d'une machine. Toutes les ressources sont accessible via [GitHub](https://github.com/prusa3d).

![](../images/module-03/prusa-farm-parts.jpg)

L'imprimante possède un bras sur qui se déplace selon les axes x-y-z. La taille maximale que la machine est capable d'imprimer de 250x210x210mm, à une vitesse max. de 200 mm/s. Cependant il faut garder en tête qu'imprimer à une telle vitesse amoindrit la qualité de l'impression.  
L'imprimante est composé de plusieurs éléments :

![](../images/module-03/original-prusa-i3-mk3s-3d-printer.jpeg)

1. *bobines de filaments* : la machine prend en charge jusque 2 bobines à la fois. Il existe une multitude de type de filaments, le plus courant est le PLA.
2. *buse d'impression* : positionnée sur le bras articulé, on y place le filament qui y sera chauffé pour être déposer sur le plateau.
3. *le plateau* : c'est sur cette plaque mobile que vient se déposer la matière. Le plateau est chauffé pour permettre une meilleure adhésion de la matière. Il est nécessaire de le nettoyer à l’acétone avant chaque utilisation pour ne pas compromettre la qualité de l'impression (l'objet adhère mal et se décolle ce qui fait planter l'impression)
4. *le panneau de contrôle* : il permet de régler un certain nombre de paramètre de l'imprimante (temps d'impression, changement du filament,...). C'est aussi là que l'on choisi l'objet à imprimer depuis la carte SD.

### Installation
Bon, on en sait un peu plus sur l'imprimante mais maintenant il faut l'utiliser. D'abord il faut télécharger le logiciel compatible avec l'imprimante que tu vas utiliser. Pour l'utilisation au FabLab ULB, j'ai donc téléchargé ([ici](https://www.prusa3d.com/drivers/)) le "*Original Prusa I3 MK3S & MK3S+*" compatible avec mon système d'exploitation.

![](../images/module-03/telechargement.png)

Le logiciel Prusa ou PrusaSlicer permet de générer un fichier *G-code* que l'imprimante est capable de comprendre et d'imprimer.

### L'interface

L'interface est assez simple et intuitive.

![](../images/module-03/interface.png)

1. Ces différents onglet permettent de paramétrer l'impression : spécificité de l'impression, type de filaments et choisir l'imprimante.

2. Ces différentes icônes de gauche à droite servent à : ajouter un objet, supprimer un objet sélectionner, tout supprimer, copier, coller, dupliquer, supprimer la copie, recherche, faire varie les épaisseurs de couches, retour en arrière et rétablir.

3. On retrouve ici 3 menus déroulants pour les réglages principaux : qualité d'impression, de type de filament et de quelle imprimante.

4. Le menu de gauche permet de déplacer l'objet (de haut en bas) : déplacer sur le plateau, modifier l'échelle (peut être controlé numérique aussi dans la barre à droite après l'import d'un objet), pivoter l'objet, choisir la surface de l'objet en contact avec le plateau, choisir un plan de section pour l'objet, dessiner les supports, joindre des éléments.

5. Il s'agit tout simplement du plateau ou lit de l'imprimante, l'objet doit être positionner dessus.

6. Ces 2 icônes symbolisent pour celle de droite la zone de travail (éditeur), et celle de gauche aperçu de la découpe (affichage avec les supports et les couches).

Voilà pour la présentation dans les grandes lignes du logiciel.

## **L'impression de l'objet**

### Paramétrage de l'impression

Dans un premier temps il faut [exporter](https://knowledge.autodesk.com/fr/support/fusion-360/troubleshooting/caas/sfdcarticles/sfdcarticles/FRA/How-to-export-an-STL-file-from-Fusion-360.html) le modèle au format .stl depuis Fusion360. Le modèle que j'utilise est disponible via le lien au-dessus. Ensuite on importe le modèle dans le logiciel. Un message d'erreur s'affiche et l'objet est en bleu. Le modèle est beaucoup trop grand pour l'impression.

![](../images/module-03/01.png)

Il faut alors jouer avec l'échelle. En passant de 100% à 10%, l'objet passe en vert. On est bon.

![](../images/module-03/02.png)

Ensuite je configure les paramètres généraux d'impression (à droite) comme ci-dessous, en sélectionnant la bonne imprimante le bon type de filamenent et en sélectionnant les supports partout pour mon objet.

![](../images/module-03/03.png)

Dans l’onglet *'réglage d'impression'*, je règle les paramètres de couches et périmètres. S'agissant une impression test et à taille réduite, je règle le périmètre des parois verticales et horizontales sur 2, (par défaut le réglage est à 3).

![](../images/module-03/04.png)

Pour ce qui est du remplissage, l'objet ne demandant pas beaucoup de remplissage je laisse les paramètres par défaut. Idem pour les supports.

![](../images/module-03/05.png)
![](../images/module-03/06.png)

On passe donc maintenant à l'export du g-code soit en cliquant sur l'icône de droite du menu en bas à gauche, ou en cliquant sur *'découper maintenant'* en bas à droite.
Le modèle s'affiche maintenant en couches successives. Les éléments en vert sont les supports, en orange on voit l'objet et en rouge c'est le remplissage. (en haut à gauche on retrouve une légende de tout les éléments). A cette étape on contrôle le temps d'impression, la consigne était de ne pas dépasser 2h, ici le temps estimé est de 1h34. Tout bon !  

![](../images/module-03/07.png)
![](../images/module-03/08.png)

On peut exporter le g-code et le glisser sur la carte SD de l'imprimante.

### Préparation de l'impression

Après ça, plusieurs étapes préalable au lancement de l'impression :

 * Vérifier qu’il y ait assez de filaments sur la bobine (si ce n’est pas le cas ou que l'on veut d'autres matériaux ou couleurs on la change). J’ai choisi d’utiliser du PLA de couleur rouge pour se rapprocher de l’objet original.
 * Je nettoie le plateau aimanté comme expliqué dans la présentation de l’imprimante.
 * J’ouvre mon fichier que j’ai placé sur la carte SD depuis le panneau de contrôle.
 * Je contrôle les informations de l'impression : températures, temps d'impression et vitesse d'impression (il est possible d'accélérer la vitesse d'impression par 50% d'incrément max. jusque 300% après le lancement pour ne pas affecter la qualité d'impression).

Tout est bon, l'impression se lancera quand le plateau et la buse auront atteint la bonne température.

 ***Il important de rester proche de la machine pendant les premières couches pour s'assurer que les paramètres choisit sont bons et que tout se passe bien.***

### Première impression

L'impression de mon objet s'est globalement bien passé. Au cours de l'impression j'ai accéléré la vitesse à 150%. L'impression a donc duré 1h15 au lieu des 1h34 initiale.

![](../images/module-03/objet.JPG)
![](../images/module-03/objet2.JPG)

J'ai cependant rencontrer un autre problème lié à un mauvais paramétrage. Les support de l'objet était beaucoup trop dense ce qui a rendu leur retrait **très** compliqué et le résultat n'est pas propre du tout.

### Deuxième impression

J'ai donc chercher un solution à ce problème. En réalité la solution est hyper simple et il s'agit plutôt de l'inattention de ma part qu'un problème d'impression.
Dans PrusaSlicer il faut bien veiller à sélectionner le mode ***'expert'***

![](../images/module-03/09.png)

En activant ce mode, un tas d'autres paramètre devienne disponible et on peut choisir la densité des supports. Je passe a une structure en nid d'abeille et un espace de motifs de 5mm pour avoir le moins de densité.

![](../images/module-03/10.png)
![](../images/module-03/11.png)
![](../images/module-03/12.png)

Ce que je vois me convient. Je décide donc d'imprimer.

Pour cette 2ème impression j'ai decidé d'imprimer l'objet à 2 échelles différentes ; la petite à 10% comme la première fois, et la plus grande à 45%. Evidement l'impression était plus longue *(environ 8h réduite à 5h en augmentant la vitesse d'impression)* mais je voulais voir à quel point la taille de l'objet impactait la proprété du rendu. Et voilà le résultat.

![](../images/module-03/double.JPG)

Le rendu du plus petit modèle est pas mal brouillon.

- Petit Modèle

![](../images/module-03/petit.JPG)  ![](../images/module-03/petit3.JPG)

- Grand Modèle

![](../images/module-03/gd3.JPG)   ![](../images/module-03/gd1.JPG)


## **Liens Utiles**

- [Modèle .stl](../images/module-02/update/porte-revue-paramétré-final.stl)
- [GitHub de Prusa 3D](https://github.com/prusa3d)
- [Télécharger PrusaSlicer](https://www.prusa3d.com/drivers/)
- [Tutoriel Export format .stl depuis Fusion360](https://knowledge.autodesk.com/fr/support/fusion-360/troubleshooting/caas/sfdcarticles/sfdcarticles/FRA/How-to-export-an-STL-file-from-Fusion-360.html)
