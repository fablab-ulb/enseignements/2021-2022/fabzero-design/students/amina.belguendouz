# **MODULE 04_Découpe Assistée Par Ordinateur**

Ces deux dernière semaines, nous nous sommes intéressé à la découpe laser. Ce module se découpe en 2 temps. D'abord la familiarisation avec ce qu'est la découpe assisté par ordinateur et le fonctionnement des machines du FabLab de l'ULB. Ensuite la création d'une lampe pour enfant au moyen de la découpe laser uniquement.

## Qu'est-ce que c'est la découpe assistée par ordinateur ?
Bon, la découpe assisté par ordinateur c'est la reproduction d'un dessin transmis à la machine qui va ensuite le découper, le graver, ou le marquer sur le matériaux de notre choix.

![](../images/module-04/gravure-decoupe-marquage.png)

Pour que la machine puisse découper le dessin, il doit être vectoriel. Un dessin vectoriel s'oppose au dessin matriciel. Ce dernier est composé d'un ensemble de pixels. Une image est donc une sorte de carte de points colorés juxtaposés. Ceci veut dire que la résolution est fixe et que si l'on zoom, on se retrouve avec une image floue (partie gauche de l'image ci-dessous). Un dessin vectoriel lui est composé de ligne géométrique dès lors la résolution possible est infini (partie droite de l'image).

![](../images/module-04/vectoriel-matricielle.png)

Pour la découpe laser ce qui nous intéresse est le dessin vectoriel. Les machines ne comprennent pas les contours d'une image matricielle. Pour pourvoir découper un modèle, il faut donc veiller à vectoriser l'image que l'on veut.  

## Les machines du FabLab

Le FabLab de l'ULB possède 2 machines : la Lasersaur et l'EpilogFusion. Elles sont capables de faire le même travail cependant elles fonctionnent différemment.  

### Les précautions avant l'utilisation

Avant de lancer une découpe, il est important de connaitre les risques et les précautions a prendre pour les éviter.

```
Pour éviter tout risque d'incendie :

- Connaître avec certitude quel matériau est découpé
- Toujours activer l’air comprimé !
- Toujours allumer l’extracteur de fumée !
- Savoir où est le bouton d’arrêt d’urgence
- Savoir où trouver un extincteur au CO2
- Rester à proximité de la machine jusqu'à la fin de la découpe

Pour votre santé :

- Ne pas regarder fixement l'impact du faisceau LASER
- Après une découpe, ne pas ouvrir la machine tant qu'il y a de la fumée à l'intérieur

(tiré du guide FabLab)
```

Pour plus d'infos, ici le [guide Lasercutters du FabLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md)

### Lasersaur

![](../images/module-04/lasersaur.JPG)

**Spécifications**

* Surface de découpe : 122 x 61 cm
* Hauteur maximum : 12 cm
* Vitesse maximum : 6000 mm/min
* Puissance du LASER : 100 W
* Type de LASER : Tube CO2 (infrarouge)

Il s'agit d'une machine en open-source conçu par le FabLab. Cette machine, par sa taille, permet de travailler sur une grande surface et faire des découpes de qualité grâce à sa puissance. Cependant il est déconseillé de faire des découpes à pleine puissance ou vitesse car la qualité en serait amoindrit. L'interface de communication est la Drive BoardApp. Cette interface gère les fichiers de type SVG ou DXF. Les fichiers SVG ont tendance à mieux fonctionner généralement.

Voilà [le manuel élaboré par le FabLab pour l'utilisation du Lasersaur en toute sécurité](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md).

### EpilogFusion

![](../images/module-04/fusionpro.JPG)

**Spécifications**

* Surface de découpe : 81 x 50 cm
* Hauteur maximum : 31 cm
* Puissance du LASER : 60 W
* Type de LASER : Tube CO2 (infrarouge)

Cette machine est plus petite que la Lasersaur et utilise Inkscape comme interface de communication.  
Voilà [le manuel élaboré par le FabLab pour l'utilisation de l'EpilogFusion](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md).

## Première découpe : le calibrage

Le première exercice pour nous familiariser avec les machines et de savoir comment la paramétrer pour avoir le résultat qu'on recherche.

##### Découpe

Pour le calibrage de la découpe, on a récupéré [ce fichier de base](../images/module-04/calibration_grid.svg)
Puis il a fallu paramétrer chacune des couleurs de découpe selon la graduation défini. Pour exemple, le premier carré rouge ci-dessous doit être définis avec une vitesse de 10% pour une puissance de 4%.

![](../images/module-04/calibration-grid.png)

![](../images/module-04/calibration_decoupe.jpg)

##### Pliage

Pour le pliage, on créé un [dessin](../images/module-04/calibration_grid_pliage.svg)  sur le même même principe. Seule différence, on a garder une vitesse identique 75% et on a modulé la puissance allant de 10 à 55%.

![](../images/module-04/calibration-grid-pliage.png)

![](../images/module-04/pliage.jpg)

## La lampe pour enfant

L'exercice suivant propose de réaliser une lampe pour enfant. Cette lampe devait être réaliser sur une feuille polypropylène, un plastique translucide, et sans autre matière. Autre contrainte, avoir le minimum de perte de matière.

### Esquisses et test papier

Pour commencer, j'ai réalisé quelques esquisses. Mes premières idées étaient d'avoir une lampe qui serait modulable, c'est-à-dire, avec laquelle on puisse interagir et qui puisse changer de forme. Puis à la suite de test papier très peu fructueux, je me suis diriger sur la voie de la veilleuse. Donc d'une lampe qui pourrait avoir des projections sur les murs et plafond.

![](../images/module-04/combine.png)

### Premier test en polypropylène - découpe à la main

![](../images/module-04/lampe-test-poly.JPG)

Avec ce premier test avec le polypropylène, cette forme qui semble en déséquilibre me plaisait bien. Il restait alors à définir des formes qui se projetteraient.

### Découpe sur l'Epilog

#### Le dessin

Je suis parti dans l'idée d'une illusion d'optique de [Vaserely](../images/module-04/illusion-vaserely.jpeg) qui se déploierait sur la longueur de la bande. J'ai également fait un test avec des liserés.
Pour le fichier de découpe, j'ai dessiné sur ArchiCad avec les outils de dessin 2D (équivalent d'AutoCad). Ça permet de s'assurer de desssiner avec les bonnes dimensions.

![](../images/module-04/dessin.png)

Ensuite j'exporte le document en .dwg pour l'ouvrir avec Illustrator (j'aurais pu utiliser Inkscape mais Illustrator est déjà installé sur mon ordi et puis je le connait déjà)

![](../images/module-04/export-dwg.png)

A l'ouverture du fichier il faut veiller à cocher *'taille originale'*.

![](../images/module-04/import-illustrator.png)

On retrouve ensuite le dessin Illustrator. Maintenant il faut gérer les calques. Les tracés doivent être hiérarchisés en priorité de découpe. Ici, j'ai trois couleurs de découpes en commençant par l'intérieur des surfaces, puis les cercles d'accroche à l'ampoule et en finissant par le contour de la bande.

![](../images/module-04/gestion-calque.png)

En voilà le fichier de découpe prêt. Il ne reste qu'à l'exporter en .svg pour qu'il soit lisible sur Inkscape et le glisser sur une clé USB.

![](../images/module-04/export-svg.png)

#### La découpe

Une fois le fichier ouvert avec Inkscape sur l'ordi de la machine, on verifie que les épaisseurs de lignes : pour avoir une découpe fine les lignes doivent avoir une épaisseur de 0,01 mm.

![](../images/module-04/epaisseur.JPG)

***Petite remarque, si tu travailles sur Illustrator initialement et que tu transfert ton fichier .svg sur Inkscape prête attention à l'échelle de ton dessin avant de l'imprimer. J'ai remarqué une réduction alors que le dessin devait prendre l'ensemble de la feuille de polypropylène, là il n'en recouvrait qu'une partie.***

Une fois que tout est bon. On envoie à l'impression en s'assurant que l'Epilog soit bien séléctionnée.

![](../images/module-04/imprimer.JPG)

L'interface machine s'ouvre alors et on peut commencer à paramétrer l'ensemble. Il faut d'abord séléctionner *'pluger'* ou *'palpeur'* (1) en français. Ensuite éclater le dessin par calque de couleur en cliquant sur le ''+ couleur'(2). Après il faut veiller à programmer le bon process, ici *'vector'* ou *'découpe'* (3) en français pour la découpe. Pour paramétrer la puissance et la vitesse, on se réfère au test de calibrage fait au préalable. Ici, j'ai choisi une vitesse de 10% pour une puissance de 20% et en ajustant la fréquence à 100%. (4) Ces paramètres été choisit pour chacun des 3 calques (5) (je n'ai que des découpes). Ensuite on les met dans l'ordre de découpe. Enfin on peut imprimer (en bas à droite).(6)

1. ![](../images/module-04/parametre1.JPG)

2. ![](../images/module-04/parametre2.JPG)

3. ![](../images/module-04/parametre3.JPG)

4. ![](../images/module-04/parametre4.JPG)

5. ![](../images/module-04/parametre5.JPG)

6. ![](../images/module-04/imprimer-epilog.JPG)

Pour finir, on séléctionne sur l'écran de la machine le fichier qu'on envoie à la découpe puis on clique sur le bouton *'play'* en ayant veiller au préalable que l'aspiration d'air soit allumer.

![](../images/module-04/bouton.JPG)

#### Résultat de la découpe

![](../images/module-04/decoupe.JPG)

![](../images/module-04/chute.JPG)

### Assemblage de la lampe

1. ![](../images/module-04/00.JPG)

2. ![](../images/module-04/01.JPG)

3. ![](../images/module-04/02.JPG)

4. ![](../images/module-04/03.JPG)

5. ![](../images/module-04/04.JPG)

6. ![](../images/module-04/05.JPG)

7. ![](../images/module-04/06.JPG)

### Le résultat

![](../images/module-04/lampe-source-forte.JPG)

Bon, je ne suis pas satisfaite du résultat. Les projections sur les murs et plafonds sont inexistantes. Ça semble être dû à l'ampoule beaucoup trop forte qui ébloui et qui traverse les surfaces pleines et trouées de la même manière. Par contre avec une source lumineuse plus faible, on arrive à percevoir des projections.

![](../images/module-04/lampe-source-faible.JPG)

## Liens Utiles
- [Télécharger Inkscape](https://inkscape.org/fr/)
- [Guide LaserCutters du FabLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md)
- [Guide Lasersaur](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)
- [Guide Matériaux Lasersaur](https://github.com/nortd/lasersaur/wiki/materials)
- [Guide EpilogFusion](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/EpilogFusion.md)
- [Guide Matériaux EpilogFusion](https://www.epiloglaser.com/assets/downloads/fusion-material-settings.pdf)
- [Fichier .svg de calibrage de découpe ](../images/module-04/calibration_grid.svg)
- [Fichier .svg de calibrage pliage](../images/module-04/calibration_grid_pliage.svg)
- [Fichier .svg découpe pour la lampe](../images/module-04/panneau-final.svg)
