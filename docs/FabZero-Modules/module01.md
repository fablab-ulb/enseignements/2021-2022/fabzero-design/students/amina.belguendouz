# **MODULE 01_Documentation**

Cette première séance était consacrée à la découverte de GitLab et du monde de la documentation open-source et de contrôle de version.
Cette plateforme sera le moyen pour nous de documenter et partager notre processus de travail facilement et rapidement.

## **Installation et configuration de GIT**

### Etape 1 - vérifier si GIT est installé sur mon ordinateur

En suivant les indications de ce [tutoriel](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git), j'ai déterminé si GIT existait sur mon ordinateur.
J'ai donc ouvert le terminal et taper la ligne suivante :
```
git --version
```
Le message de réponse de l'ordinateur est le suivant :

![](../images/module-01/message-erreur-outil-commande-small.jpg)

Il m'a donc fallu installer de les outils de développement de ligne de commande, sans quoi la machine ne comprends pas ce que je lui demande.

###### *[ meanwhile ]*

Le chargement étant long, j'en ai profiter pour télécharger un éditeur de texte MarkDown comme ça nous a été recommandé, ceci permettant un meilleur workflow.

J'ai choisi d'installer **Atom** et pour ça, rien de plus simple. En se rendant sur le [site](https://atom.io), le téléchargement est directement accessible.

###### *[ end ]*

Le chargement enfin terminé, j'ai pu voir, grâce à la commande précédente, que mon ordinateur avait une version de git pré-installé.

![](../images/module-01/terminal-01-small.jpg)

J'ai donc pu passer à l'étape suivante.


### Etape 2 - La configuration GIT

Toujours en suivant les étapes du [tutoriel](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git), j'ai configuré mon compte utilisateur.

![](../images/module-01/terminal-02.jpg)


### Etape 3 - La clé SSH

L'étape suivante est celle qui (*si j'ai bien compris*) nous permet de créer le lien entre le serveur et notre ordinateur. Sans celle ci, aucune des modifications faites en local ne peuvent être push.

Dans mon cas, Open Source SSH est pré-installé, il a donc fallu générer une clé par la commande suivante :

```
ssh-keygen -t ed25519 -C "<comment>"
```

![](../images/module-01/terminal-03.jpg)

La clé est alors générée. Il faut ensuite la copier la clé publique par la commande suivante :

```
tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy
```

dans l'onglet clé SSH de GitLab, accessible à partir des préférences depuis l'avatar en haut à droite.

  ![](../images/module-01/ssh-gitlab.jpg)

### Etape 4 - Cloner le répertoire

Afin de pourvoir travailler en local sur notre site et documentation, il faut en créer une copie, un clone sur l'ordinateur.

![](../images/module-01/clone.jpg)

L'URL copié depuis GitLab doit ensuite être coller dans le terminal comme suit :

![](../images/module-01/terminal-06.jpg)

Ma première tentative n'a pas été réussit, car je n'étais pas aller au bout de la procedure. J'ai ensuite réessayé en ouvrant le terminal depuis un autre dossier et cette fois-ci ça a fonctionner. La dernière est de confirmer le clone avec 'yes'

![](../images/module-01/clone-reussi.jpg)

Un dossier s'est créé à l'endroit choisi avec tout le répertoire GitLab.
Tout est donc prêt à l'emploi!

## **Atom et le MarkDown**

### Etape 1 - Installer Atom et configurer l'affichage

*Pour l'installation de Atom se référer plus haut.*

Après l'installation, j'ai importé tous le dossier tiré de GitLab dans Atom.

![](../images/module-01/atom-01.jpg)

![](../images/module-01/atom-02.jpg)

![](../images/module-01/atom-03.jpg)

Voilà tout les documents sont importés, maintenant on peut passer au modifications.

### Etape 2 - Se familiariser au MarkDown

La première chose que j'ai faite avant tout a été de chercher une [cheat sheet](https://www.markdownguide.org/cheat-sheet/) pour comprendre comment fonctionne le MarkDown. J'ai conservé ce qui m'était nécéssaire, les choses basiques :

![](../images/module-01/cheat-sheet.jpg)

### Etape 3 - Push/Pull/Fetch

Alors voilà je sais comment modifier mon texte depuis Atom c'est très bien mais bon, ces modifications doivent être communiquées au serveur pour qu'elles soient visibles de tous.

* fetch : pour savoir si la version est bien la dernière.
* pull : récupérer la dernière version qui est en ligne.
* push : envoyer les modifications faites en local.

J'ai modifié dans un premier temps la page index et voulu tester de push pour en comprendre la procédure.

1. enregistrer les modifications (*la bulle bleue de l'onglet doit disparaitre*)
2. "stage" les changements (*onglet en haut à droite*) et si on veut annuler il faut "unstage"
3. nommer le "commit" puis "commit to main"
4. push le commit

1 ![](../images/module-01/push-01.jpg)

2 ![](../images/module-01/push-02.jpg)

3 ![](../images/module-01/push-03.jpg)

4 ![](../images/module-01/push-04.jpg)

## **Personnaliser le site**

Une dernière que j'ai presque oublié, c'est celle de la personnalisation du fichier 'mkdocs.yml'
Il faut pour ce faire éditer le document soit sur Atom, soit sur GitLab en direct. J'ai opté pour la seconde option.
Il suffit de cliquer sur "Edit" lorsque qu'on ouvre le document et là modifier tous les éléments nécessaires.
Le choix du thème se fait également à cette endroit. Les différents thème que l'on peut avoir sont répertoriés sur ce [site](https://mkdocs.github.io/mkdocs-bootswatch/?fbclid=IwAR2cojBDtkeUeoi47RgVidXlxqEEEgz7H4wbAiy_CHaIIV-mO5pMfJLkgG4).

![](../images/module-01/custom-site-small.jpg)

![](../images/module-01/custom-site.jpg)

## **Update**

Afin de compresser les images à envoyer sur le git, j'ai trouvé une nouvelle méthode passant le terminal grâce à ce [tuto](https://www.youtube.com/watch?v=HVMOBKRNEbc&t=330s).

## **Liens Utiles**

- [Tutoriel GIT](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git)
- [Tutoriel Compression d'Image via le Terminal MAC (-anglais)](https://www.youtube.com/watch?v=HVMOBKRNEbc&t=330s)
- [Télécharger Atom](https://atom.io)
- [Cheat Sheet Markdown](https://www.markdownguide.org/cheat-sheet/)
- [Choix Thème Markdown](https://mkdocs.github.io/mkdocs-bootswatch/?fbclid=IwAR2cojBDtkeUeoi47RgVidXlxqEEEgz7H4wbAiy_CHaIIV-mO5pMfJLkgG4)
