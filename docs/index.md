## A propos de moi

![](images/index/photocouverturesmall.jpg)

Salut ! Je m'appelle Amina. Je suis étudiante en Master 2 à la faculté d'architecture La Cambre Horta de l'ULB. J'ai entamé mes études en 2013 mais mon parcours n'a pas vraiment été linéaire. Après l'obtention de mon Bachelier en 2017, j'ai fait une pause. Pendant trois ans, j'ai eu l'opportunité de travailler dans un petit bureau d'architecture dans la ville dans laquelle j'ai grandi en Haute-Savoie. Cette expérience m'a confrontée à la réalité du métier. Ça a été un moment riche d'apprentissage qui a conforté mon choix d'étude. L'an dernier j'ai donc décidé de reprendre mes études ( *parce qu'avoir un diplôme c'est quand même chouette* ) et donc après un total de 8 ans d'étude et de travail , me voilà enfin à la dernière ligne droite.

Passionnée de musique vous me verrez toujours *ou presque* des écouteurs aux oreilles. Touche à tout j'aime apprendre de nouvelles choses même si je m'en lasse parfois très vite.

## Quelques projets

Voilà une petite collection de mes projets, passant de l'architecture, par le dessin et l'illustration, à la photographie.

![](images/index/projets.jpg)
![](images/index/projets2.jpg)
![](images/index/projets3.jpg)

## L'objet

Après la visite du musée, mon choix s'est porté sur le porte-revue de Giotto Stoppino. Ces modules qui se répètent et s'emboitent les uns dans les autres est la première chose qui m'a attiré. C'est un objet simple mais élégant je trouve.

![](./images/module-02/giotto-stoppino-magazine-rack.jpeg)
